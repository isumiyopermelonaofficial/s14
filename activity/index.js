
// Addition
function addSum(num1, num2) {
	console.log("The Sum of " + num1 + " and " + num2);
	console.log(num1 + num2);
}

addSum(10,5);
// End of Addition


// Subtraction
function computeDiff(diff1, diff2){
	console.log("The Difference of " + diff1 + " and " + diff2);
	console.log(diff1 - diff2);
}

computeDiff(20,10);
// End of Subtraction



// Multiplication

let product

function computeProd(prod1, prod2){
	console.log("The Product of " + prod1 + " and " + prod2);
	//console.log(prod1 * prod2);
	return prod1 * prod2
}

product = computeProd(2,14)
console.log(product);
// End of Multiplication